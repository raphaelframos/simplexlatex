/*
 * InformacoesUtils.h
 *
 *  Created on: 12/08/2014
 *      Author: raphael
 */

#ifndef INFORMACOESUTILS_H_
#define INFORMACOESUTILS_H_
#include <iostream>
#include <string>
#include <sstream>
#include "ConstantsUtils.h"

using namespace std;

class InfoUtils{
public:
	InfoUtils();
	void mostraMensagemParaDetalhes(string mensagem);
	void mostraMensagemParaDetalhes();
	static int quantidadeDeMensagens;
	stringstream mensagem;

private:
	void limpaMensagem();
};



#endif /* INFORMACOESUTILS_H_ */
