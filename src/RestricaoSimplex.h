/*
 * LinhaSimplex.h
 *
 *  Created on: 30/07/2014
 *      Author: raphael
 */

#ifndef LINHASIMPLEX_H_
#define LINHASIMPLEX_H_
#include <vector>
#include <sstream>
#include <string>
#include <cstdlib>
#include <iomanip>
#include "ConstantsUtils.h"
#include "Fracoes.h"

using namespace std;

class RestricaoSimplex {

public:
	RestricaoSimplex();
	void setSinalDaRestricao(string sinal);
	string getSinalDaRestricao();
	void setValoresDaLinha(vector<Fracao> valores);
	void adicionaValor(Fracao valor);
	vector<Fracao> getValoresDaLinha();
	bool possuiVariavelDeFolga();
	bool possuiVariavelArtificial();
	bool isFuncaoObjetiva();
	bool isFuncaoObjetivaArtificial();
	bool restricaoPositiva();
	bool isFuncaoArtificial();
	bool isRestricaoTodaPositiva();
	void setFuncaoArtificial(bool);
	void setVariavelDeFolga(bool valor);
	void setVariavelArtificial(bool valor);
	int getQuantidadeDeValoresDaRestricao();
	int getQuantidadeDeVariaveisExtras();
	int getIndiceDaUltimaColunaComValorIgualAUm();
	Fracao getValorDaBase();
	string getValoresDaRestricao();

	void removeLadoDireito();
	void removeValorDaRestricaoNaPosicao(int posicao);
	void setValorDaLinha(int posicao, Fracao valor);
	void somaLinha(RestricaoSimplex linha);
	void adicionaNomeDasVariavelColunaEValor(string nome, int coluna, Fracao valor);
	string retornaNomeDasVariaveis();
	void defineNomeParaAsVariaveis(int quantidadeTotalDeVariaveisExtras, int quantidadeTotalDeVariaveisArtificiais);
	int getTamanhoRealDaRestricao();
	string getRestricaoCompleta();
	RestricaoSimplex getRestricaoArtificial();
	void atualizaValor(int posicao, Fracao valor);
	void setNomeDaBase(string nomeDaBase);
	string getNomeDaBase();
	Fracao getValor(int posicao);
private:
	vector<Fracao> valoresDaRestricao;
	string sinalDaRestricao;
	bool variavelDeFolga;
	bool variavelArtificial;
	bool funcaoObjetivaArtificial;

};

#endif /* LINHASIMPLEX_H_ */
