/*
 * Pivoteamento.h
 *
 *  Created on: 10/08/2014
 *      Author: raphael
 */

#ifndef PIVOTEAMENTO_H_
#define PIVOTEAMENTO_H_

#include <iostream>
#include <fstream>
#include <string>
#include <cstdlib>
#include <vector>
#include <exception>
#include "RestricaoSimplex.h"
#include "UtilsSimplex.h"
#include "InformacoesUtils.h"
#include "TabelaSimplex.h"

using namespace std;

class Pivoteamento{
public:
	Pivoteamento(TabelaSimplex);
	vector<TabelaSimplex> iniciaPivoteamento();
	InfoUtils infoUtils;
	void adicionaTabela(TabelaSimplex tabela);
	vector<TabelaSimplex> getTabelasFinais();
	vector<TabelaSimplex> getTabelas();
	void iniciaSegundaFase();
	bool ultimaLinhaPositiva();

private:
	Fracao pivo;
	int colunaDoPivo;
	int linhaDoPivo;
	vector<Fracao> possiveisPivo;
	RestricaoSimplex ultimaLinha;
	void defineColunaDoPivo();
	void definePivoELinhaDoPivo();
	void buscaSolucao();
	void formataLinhaDoPivo();
	RestricaoSimplex retornaUltimaLinha();
	RestricaoSimplex getLinhaPivo();
	int getQuantidadeDeLinhasSemAFuncaoObjetiva();
	int getPosicaoDaUltimaLinha();
	RestricaoSimplex multiplicaLinhaPelo(Fracao valor, RestricaoSimplex linha);
	void zeraColunaDoPivo();
	bool criterioDeParadaNaoAtingido();
	void definePorOrdemLexografica(const vector<int>& linhasCandidatasAPivo);
	void defineLinhaDoPivo(const vector<int>& linhasCandidatasAPivo);
	void defineQuemEntraNaBase();
	void defineQuemSaiDaBase();
	void defineBaseInicial();
	void mostraSolucao();
	void criaTabelaParaLatex(TabelaSimplex tabelaSimplex);






	vector<TabelaSimplex> tabelasFinais;
	TabelaSimplex tabelaSimplex;

};





#endif /* PIVOTEAMENTO_H_ */
