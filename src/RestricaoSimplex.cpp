/*
 * linhaSimplex.cpp
 *
 *  Created on: 30/07/2014
 *      Author: raphael
 */
#include "RestricaoSimplex.h"

RestricaoSimplex::RestricaoSimplex() {
	this->sinalDaRestricao = "";
	this->variavelArtificial = false;
	this->variavelDeFolga = false;
	this->funcaoObjetivaArtificial = false;
}

bool RestricaoSimplex::isFuncaoObjetivaArtificial() {
	if (possuiVariavelArtificial()) {
		return true;
	}

	if(isFuncaoArtificial()){
		return true;
	}

	return false;
}

bool RestricaoSimplex::restricaoPositiva(){
	for(int i=0;i<getQuantidadeDeValoresDaRestricao();i++){
		if(getValor(i).isNegativo()){
			return false;
		}
	}
	return true;
}

bool RestricaoSimplex::isFuncaoArtificial(){
	return funcaoObjetivaArtificial;
}
void RestricaoSimplex::setFuncaoArtificial(bool valor){
	this->funcaoObjetivaArtificial = valor;
}

int RestricaoSimplex::getIndiceDaUltimaColunaComValorIgualAUm() {
	int ultimaColunaPossivel = getValoresDaLinha().size() -2;
	for (int i = getValoresDaLinha().size() -2; i > 0; i--) {
		if (getValoresDaLinha()[i].isIgual(Fracao(1, 1))) {
			return i;
		}
	}
	return ultimaColunaPossivel;
}

RestricaoSimplex RestricaoSimplex::getRestricaoArtificial() {
	RestricaoSimplex novaRestricao;
	novaRestricao.setValoresDaLinha(getValoresDaLinha());
	if (possuiVariavelArtificial()) {
		int tamanhoDaLinha = novaRestricao.getValoresDaLinha().size();
		for (int i = 0; i < tamanhoDaLinha-1; i++) {
			if (i== getIndiceDaUltimaColunaComValorIgualAUm()) {
				novaRestricao.setValorDaLinha(i, Fracao(0, 1));
			} else {
				novaRestricao.setValorDaLinha(i,
						getValoresDaLinha()[i].multiplica(Fracao(-1, 1)));
			}
		}
	}

	return novaRestricao;
}
void RestricaoSimplex::atualizaValor(int posicao, Fracao valor) {
	valoresDaRestricao[posicao] = valor.getValor();
}
void RestricaoSimplex::setSinalDaRestricao(string sinal) {
	this->sinalDaRestricao = sinal;
}

Fracao RestricaoSimplex::getValor(int posicao) {
	return valoresDaRestricao[posicao];
}

string RestricaoSimplex::getSinalDaRestricao() {
	return this->sinalDaRestricao;
}

void RestricaoSimplex::setValoresDaLinha(vector<Fracao> valores) {
	this->valoresDaRestricao = valores;
}

vector<Fracao> RestricaoSimplex::getValoresDaLinha() {
	return this->valoresDaRestricao;
}

void RestricaoSimplex::adicionaValor(Fracao valor) {
	this->valoresDaRestricao.push_back(valor);
}

void RestricaoSimplex::setVariavelDeFolga(bool valor) {
	this->variavelDeFolga = valor;
}

void RestricaoSimplex::setVariavelArtificial(bool valor) {
	this->variavelArtificial = valor;
}

bool RestricaoSimplex::possuiVariavelArtificial() {
	return this->variavelArtificial;
}

bool RestricaoSimplex::possuiVariavelDeFolga() {
	return this->variavelDeFolga;
}

bool RestricaoSimplex::isRestricaoTodaPositiva(){
	for(unsigned int i=0;i<valoresDaRestricao.size()-1;i++){
		if(valoresDaRestricao[i].getResultado() < 0){
			return false;
		}
	}
	return true;
}
int RestricaoSimplex::getQuantidadeDeValoresDaRestricao() {
	int tamanhoDaLinha = getValoresDaLinha().size();
	return tamanhoDaLinha;
}

int RestricaoSimplex::getTamanhoRealDaRestricao() {
	int tamanhoDaLinha = getValoresDaLinha().size();
	return tamanhoDaLinha;
}

bool RestricaoSimplex::isFuncaoObjetiva() {
	return sinalDaRestricao.empty();
}

Fracao RestricaoSimplex::getValorDaBase() {
	return valoresDaRestricao[(valoresDaRestricao.size() - 1)];
}

int RestricaoSimplex::getQuantidadeDeVariaveisExtras() {
	int quantidadeDeVariaveis = 0;
	if (variavelArtificial) {
		quantidadeDeVariaveis++;
	}
	if (variavelDeFolga) {
		quantidadeDeVariaveis++;
	}
	return quantidadeDeVariaveis;
}

string RestricaoSimplex::getValoresDaRestricao() {
	std::stringstream linha;
	ConstantsUtils constantsUtils;
	for (unsigned int i = 0; i < valoresDaRestricao.size(); i++) {
		linha << valoresDaRestricao[i].getValorDemonstracao()
				<< setw(constantsUtils.getEspacoParaValores());
	}
	return linha.str();
}

string RestricaoSimplex::getRestricaoCompleta() {
	std::stringstream linha;
	ConstantsUtils constantsUtils;
	for (unsigned int i = 0; i < valoresDaRestricao.size(); i++) {
		if(i == (valoresDaRestricao.size()-1) && !sinalDaRestricao.empty()){
			linha << sinalDaRestricao
							<< setw(constantsUtils.getEspacoParaValores());
		}
		linha << valoresDaRestricao[i].getValorDemonstracao()
				<< setw(constantsUtils.getEspacoParaValores());
	}
	return linha.str();
}

void RestricaoSimplex::removeLadoDireito() {
	valoresDaRestricao.erase(valoresDaRestricao.begin() + (valoresDaRestricao.size() - 1));
}

void RestricaoSimplex::removeValorDaRestricaoNaPosicao(int posicao){
	valoresDaRestricao.erase(valoresDaRestricao.begin() + posicao);
}

void RestricaoSimplex::setValorDaLinha(int posicao, Fracao valor) {
	valoresDaRestricao[posicao] = valor;
}

void RestricaoSimplex::somaLinha(RestricaoSimplex linha) {
	for (int i = 0; i < linha.getTamanhoRealDaRestricao(); i++) {
		setValorDaLinha(i,
				(valoresDaRestricao[i].soma(linha.getValoresDaLinha()[i])));
	}
}
