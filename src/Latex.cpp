/*
 * Latex.cpp
 *
 *  Created on: 09/09/2014
 *      Author: raphael
 */

#include "Latex.h"

string Latex::defineSinalDaVariavel(Fracao variavel) {
	if (variavel.isPositivo()) {
		return "+";
	}
	return "";
}

string Latex::formataVariaveis(TabelaSimplex tabela, int posicaoRestricao,
		int posicaoVariavel) {
	stringstream resultado;
	Fracao variavel = tabela.getValorDaRestricaoNaPosicao(posicaoRestricao,
			posicaoVariavel);
	if (variavel.isPositivo() && posicaoVariavel != 0) {
		resultado << "+";
	}
	if (!variavel.isIgual(Fracao(0, 1))) {
		if (!variavel.isIgual(Fracao(1, 1))) {
			if (variavel.isIgual(Fracao(-1, 1))) {
				resultado << "-";
			} else {
				resultado << variavel.getValorLatex() << endl;
			}
		}

		resultado << tabela.getRotuloDaVariavel(posicaoVariavel);
	}

	return resultado.str();
}

void Latex::criaArquivo(vector<TabelaSimplex> tabelasFinais) {
	ofstream arquivo;
	stringstream texto;
	arquivo.open("resultado.tex");

	texto
			<< "\\documentclass[12pt,oneside,a4paper]{report}\n\\usepackage[brazil]{babel} \n\\usepackage[utf8]{inputenc}"
					"\n\\usepackage{amsmath}\n\\usepackage{amsfonts}\n\\usepackage{amssymb}\n\\usepackage{amsthm}\n\\usepackage{comment}"
					"\n\\usepackage{wrapfig}\n\\usepackage{arial}\n\\usepackage{algorithmic}\n\\usepackage[hang,bf,labelsep=space]{caption}"
					"\n\\usepackage{verbatim}\n\\usepackage{geometry}\n\\usepackage{graphicx}\n\\usepackage{latexsym}"
					"\n\\usepackage{color,times}\n\\usepackage{rotating}\n\\usepackage{setspace}\n\\usepackage{fancyhdr}"
					"\n\\usepackage[ruled,algochapter]{algorithm2e}\n\\usepackage{titlesec,titletoc}\n\\usepackage{type1cm,type1ec}"
					"\n\\usepackage{float}\n\n";
	texto
			<< "\\newcommand{\\mi}{{\\textsl{minimizar}}\\ \\ } \n\\newcommand{\\ma}{{\\textsl{maximizar}}\\ \\ } \n"
					"\\newcommand{\\su}{{\\textsl{sujeito a}}\\ \\ }\n\\newcommand{\\miz}{{\\textsl{min}}\\ \\ } "
					"\n\\newcommand{\\maz}{{\\textsl{max}}\\ \\ }\n";
	texto << "\\begin{document}\n";
	texto << "\n\\newcommand{\\hy}{\\hspace{-0.13in}}";
	texto << "\\begin{center}\n\\textbf{GALDIPLEX}\n\\end{center}\n";

	stringstream numeroDeIteracoes;
	numeroDeIteracoes << "\nNúmero de interações: "
			<< (tabelasFinais.size() - 2) << "\n\n";
	texto << numeroDeIteracoes.str();

	texto
			<< "\n\\begin{minipage}[ht]{.48\\textwidth}\n\\setlength\\arraycolsep{0.1em}\n$\n\\begin{array}{*{8}r}\n"
					"\\\\\\mi\\ &z&=&";
	TabelaSimplex tabelaInicial = tabelasFinais[0];

	for (unsigned int i = 0; i < tabelaInicial.getRestricoes().size(); i++) {
		if (i == 1) {
			texto << "\\su\\ & & &";
		} else if (i > 1) {
			texto << " & & & ";
		}

		for (int j = 0; j < tabelaInicial.getQuantidadeDeValores(i); j++) {
			if (i == 0) {
				texto << formataVariaveis(tabelaInicial, i, j);
				if (j == tabelaInicial.getQuantidadeDeValores(i) - 1) {
					texto << "\\\\\n";
				} else {
					texto << "&";
				}
			} else {
				if (j == tabelaInicial.getQuantidadeDeValores(i) - 1) {
					texto
							<< tabelaInicial.getValorDaRestricaoNaPosicao(i, j).getValorLatex();
					texto << "\\\\\n";
				} else {
					texto << formataVariaveis(tabelaInicial, i, j);
					if (j == tabelaInicial.getQuantidadeDeValores(i) - 2) {
						texto
								<< defineSinalDaRestricao(
										tabelaInicial.getRestricoes()[i]);
					}
					texto << " & ";
				}
			}
		}
	}

	texto << "\n& & & \\multicolumn{";
	texto << 5;
	texto
			<< "}{l}{\\phantom{-}x_j \\geqslant 0,\\ j=1,2}\n\\end{array}\n$\n\\end{minipage}";

	texto << criaTabelaComVariaveisExtras(tabelasFinais[1]);
	texto << criaTabelas(tabelasFinais);
	texto
			<< "\\begin{minipage}{0.45\\textwidth}\n\\begin{center}\nSolução ótima:\n\\begin{tabular}{ll}\n\n\\\\";

	texto << tabelasFinais[tabelasFinais.size() - 1].getSolucao();
	texto << "\\end{tabular}\n\\end{center}\n\\end{minipage}";
	texto << "\n\\end{document}\n";
	arquivo << texto.str() << endl;

	arquivo.close();

}

string Latex::defineQuantidadeDeColunasC(int quantidade) {
	stringstream resultado;
	for (int i = 0; i <= quantidade; i++) {
		resultado << "c";
	}
	return resultado.str();
}

string Latex::criaTabelas(vector<TabelaSimplex> tabelas) {
	stringstream texto;

	for (unsigned int i = 2; i < tabelas.size(); i++) {

		TabelaSimplex tabelaSimplex = tabelas[i];
		texto
				<< "\\begin{minipage}[t]{0.48\\textwidth}\n\\begin{center}\n$\n\\arraycolsep=0.07in\n\\begin{array}";
		texto << "{"
				<< defineQuantidadeDeColunasC(
						tabelas[i].getRotulosDasVariaveis().size())
				<< "}   \\hline\\hline\n\n";

		for(int j=0;j<tabelas[i].getQuantidadeDeValoresdaMaiorRestricao();j++){
			if(j == tabelas[i].getQuantidadeDeValoresdaMaiorRestricao()-1){
				texto << " &  \\\\\\hline";
			}else{
				texto << " &" << tabelas[i].getRotuloDaVariavel(j) << " ";
			}
		}

		for (unsigned int l = 0; l < tabelaSimplex.getRestricoes().size();
				l++) {
			if (l < tabelaSimplex.getBases().size()) {
				texto << "\n" << tabelaSimplex.getRotuloDaVariavel(tabelaSimplex.getBases()[l]) <<
						"&";
			} else {
				texto << " & \\hy ";
			}

			for (int m = 0; m < tabelaSimplex.getQuantidadeDeValores(l); m++) {
				if (m == tabelaSimplex.getQuantidadeDeValores(l) - 1
						&& l == tabelaSimplex.getRestricoes().size() - 1) {
					texto << "z ";
					if (tabelaSimplex.getValorDaRestricaoNaPosicao(l, m).isPositivo()) {
						texto << "+";
					}
				}

				texto
						<< tabelaSimplex.getValorDaRestricaoNaPosicao(l, m).getValorLatex();
				if (m == tabelaSimplex.getQuantidadeDeValores(l) - 1) {
					texto << "\\\\\\hline";
				} else {
					texto << " & ";
				}
			}
		}
		texto << "\n\\end{array}$\n\\end{center}\n\\end{minipage}\\hfill";
	}
	return texto.str();
}

string Latex::criaTabelaComVariaveisExtras(TabelaSimplex tabela) {
	stringstream texto;
	texto << "\\\\\\\\";
	texto
			<< "\n\\begin{minipage}[ht]{.48\\textwidth}\\setlength\\arraycolsep{0.1em}$\\begin{array}{*{9}r}\n";
	texto << "\\\\\\mi\\ &z&=&";
	int quantidadeDeRestricoes = tabela.getRestricoes().size();
	int quantidadeDeValoresNaRestricao =
			tabela.getQuantidadeDeValoresdaMaiorRestricao();
	if (tabela.existeFuncaoObjetivaArtificial()) {
		quantidadeDeRestricoes--;
	}
	for (int i = 0; i < quantidadeDeRestricoes; i++) {
		if (i == 1) {
			texto << "\\su\\ & & &";
		} else if (i > 1) {
			texto << " & & & ";
		}
		for (int j = 0; j < quantidadeDeValoresNaRestricao; j++) {
			if (j != quantidadeDeValoresNaRestricao - 1) {
				texto << formataVariaveis(tabela, i, j);
			}
			if (j == quantidadeDeValoresNaRestricao - 1) {
				if (i == quantidadeDeRestricoes - 1) {
					texto << " z ";
				} else {
					texto
							<< tabela.getValorDaRestricaoNaPosicao(i, j).getValorLatex();
				}
				texto << " \\\\\n";
			} else if (j == tabela.getQuantidadeDeValores(1) - 2) {
				texto << " = ";
			} else {
				texto << " &";
			}
		}
	}
	texto
			<< "\\multicolumn{9}{l}{\\phantom{-}x_j \\geqslant 0,\\ j=1,\\ldots,4}"
					"\\end{array}$\\end{minipage}\\\\[0.1in]";
	return texto.str();
}

string Latex::defineSinalDaRestricao(RestricaoSimplex restricao) {

	if (restricao.getSinalDaRestricao().compare(">=") == 0) {
		return "\\geqslant";
	} else if (restricao.getSinalDaRestricao().compare("<=") == 0) {
		return "\\leqslant";
	} else {

		return restricao.getSinalDaRestricao();
	}
}

