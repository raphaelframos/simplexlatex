/*
 * Matricial.cpp
 *
 *  Created on: 22/08/2014
 *      Author: raphael
 */

#include "Matricial.h"

void Matricial::iniciaTratamentoMatricial(ifstream &arquivo){
	UtilsSimplex utilsSimplex;
	utilsSimplex.lerArquivoSeparandoNumerosSinais(this->tabela, arquivo);
	utilsSimplex.organizaTabela(tabela);
}


