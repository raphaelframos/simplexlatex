/*
 * UtilsSimplex.h
 *
 *  Created on: 01/08/2014
 *      Author: raphael
 */

#ifndef UTILS_H_
#define UTILS_H_
#include <vector>
#include <iostream>
#include <fstream>
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include "RestricaoSimplex.h"

using namespace std;

class UtilsSimplex {
public:
	void split(vector<string> &elementos, string texto, string delimiter);
	void criaTokens(vector<string> &elementos, string texto, string parametro);
	bool isNumber(string texto);
	float stringToFloat(string texto);
	Fracao stringToFracao(string texto);
	bool isMaxOuMin(string texto);
	bool isSinal(string texto);
	bool isSinal(string texto, string sinal);
	float divideValores(float numerador, float denominador);
	void lerArquivoSeparandoNumerosSinais(vector<RestricaoSimplex> &tabela, ifstream &arquivo);
	void encontraVariaveisExtrasNa(vector<RestricaoSimplex> &tabela);
	void adicionaVariaveis(vector<RestricaoSimplex> &tabela);
	void adicionaVariavelDeFolga(RestricaoSimplex* ponteiroLinha);
	void adicionaVariavelArtificial(RestricaoSimplex* ponteiroLinha);
	int retornaTamanhoDaMaiorRestricao(vector<RestricaoSimplex> tabela);
	int retornaQuantidadeDeVariaveisExtrasNasLinhasAnteriores(vector<RestricaoSimplex> tabela, int linhaLimite);
	void preencheComZeroOValorDasVariaveisAnteriores(int quantidadeDeVariaveisExtrasAnteriores, RestricaoSimplex* ponteiroLinha);
	void preencheComZeroEspacoesRestantes(int tamanhoDaMaiorLinha, RestricaoSimplex* ponteiroLinha);
	void adicionaBase(Fracao valorDaBase, RestricaoSimplex* ponteiroLinha);
	void nomeiaVariaveis(vector<RestricaoSimplex> &tabela);
	int retornaQuantidadeTotalDeVariaveisArtificiais(vector<RestricaoSimplex> tabela);
	void mostraTabela(vector<RestricaoSimplex> tabela);
	void organizaTabela(vector<RestricaoSimplex> &tabela);
	vector<RestricaoSimplex> defineRestricoesSeparandoNumerosESinais(ifstream &arquivo);
	string intToString(int);
	string floatToString(float);
private:
	void adicionaLinha(vector<RestricaoSimplex>& tabela, RestricaoSimplex& novaLinha);
	int retornaQuantidadeDeVariaveisExtras(const vector<RestricaoSimplex>& tabela);
	void adicionaFuncaoObjetiva(
			const vector<RestricaoSimplex>& restricoesArtificias,
			vector<RestricaoSimplex>& tabela);
};

#endif /* UTILS_H_ */
