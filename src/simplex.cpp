/*
 * simplex.cpp
 *
 *  Created on: 30/07/2014
 *      Author: raphael
 */

#include "Simplex.h"
#include "RestricaoSimplex.h"
#include "Pivoteamento.h"
#include <exception>

void Simplex::adicionaLinha(RestricaoSimplex linha) {
	this->tabela.push_back(linha);
}

vector<RestricaoSimplex>& Simplex::getTabela() {
	return this->tabela;
}

void Simplex::lerArquivoSeparandoNumerosSinais(ifstream& arquivo) {
	UtilsSimplex utils;
	vector<string> vetorComElementos;
	while (!arquivo.eof()) {
		for (std::string linha; getline(arquivo, linha);) {
			string::iterator it;
			utils.split(vetorComElementos, linha, " ");
			RestricaoSimplex novaLinha;
			for (unsigned int i = 0; i < vetorComElementos.size(); i++) {
				if (utils.isNumber(vetorComElementos[i])) {
					novaLinha.adicionaValor(
							utils.stringToFracao(vetorComElementos[i]));
				} else {
					if (utils.isSinal(vetorComElementos[i])) {
						novaLinha.setSinalDaRestricao(vetorComElementos[i]);
					}
				}
			}
			adicionaLinha(novaLinha);
			vetorComElementos.clear();
		}
	}
}

RestricaoSimplex& Simplex::ultimaLinha() {
	return tabela[tabela.size() - 1];
}

Fracao Simplex::retornaElementoDaTabela(int linha, int coluna) {
	return tabela[linha].getValoresDaLinha()[coluna];
}

//Salva tabela inicial no vetor de tabelas finais
void Simplex::salvaProblema(ifstream& arquivo) {
	TabelaSimplex tabelaSimplex;
	UtilsSimplex utilsSimplex;
	tabelaSimplex.setRestricoes(utilsSimplex.defineRestricoesSeparandoNumerosESinais(arquivo));
	tabelasSimplex.push_back(tabelaSimplex);
}

TabelaSimplex Simplex::retornaUltimaTabelaAdicionada() {
	return tabelasSimplex.at(tabelasSimplex.size() - 1);
}

void Simplex::iniciaResolucaoDoProblema() {
	tabelaSimplex = retornaUltimaTabelaAdicionada();
	tabelaSimplex.organizaTabela();
	tabelasSimplex.push_back(tabelaSimplex);
	tabelaSimplex = retornaUltimaTabelaAdicionada();
	Pivoteamento pivoteamento(tabelaSimplex);
	vector<TabelaSimplex> tabelasPivoteadas = pivoteamento.iniciaPivoteamento();
	for (int i = 0; i < tabelasPivoteadas.size(); i++) {
		tabelasSimplex.push_back(tabelasPivoteadas[i]);
	}
}

//O arquivo de texto é salvo no formato inicial na tabela simplex e organiza a tabela para uso futuro
vector<TabelaSimplex> Simplex::resolveTabelasSimplexComArquivoDefinidoNoTXT(ifstream &arquivo) {
	salvaProblema(arquivo);
	iniciaResolucaoDoProblema();
	return tabelasSimplex;
}

int Simplex::getTamanhoDaMaiorLinhaDaTabela() {
	int tamanhoDaLinhaComMaisValores = this->tabela[0].getQuantidadeDeValoresDaRestricao();
	RestricaoSimplex linha;
	for (unsigned int i = 1; i < this->tabela.size(); i++) {
		linha = this->tabela[i];
		if (linha.getQuantidadeDeValoresDaRestricao() > tamanhoDaLinhaComMaisValores) {
			tamanhoDaLinhaComMaisValores = linha.getQuantidadeDeValoresDaRestricao();
		}
	}

	int quantidadeDeVariaveisExtras =
			this->tabela[0].getQuantidadeDeVariaveisExtras();
	for (unsigned int i = 1; i < tabela.size(); i++) {
		linha = this->tabela[i];

		if (quantidadeDeVariaveisExtras != 0) {
			tamanhoDaLinhaComMaisValores += quantidadeDeVariaveisExtras;
		}
		quantidadeDeVariaveisExtras = linha.getQuantidadeDeVariaveisExtras();
	}
	return tamanhoDaLinhaComMaisValores;
}

int Simplex::getTotalDeVariaveisExtras() {
	int totalDeVariaveis = 0;
	RestricaoSimplex linha;
	for (unsigned int i = 0; i < tabela.size(); i++) {
		linha = tabela[i];
		totalDeVariaveis = totalDeVariaveis
				+ linha.getQuantidadeDeVariaveisExtras();
	}
	return totalDeVariaveis;
}

int Simplex::quantidadeTotalDeVariaveisArtificiais() {
	int totalDeVariaveisArtificiais = 0;
	RestricaoSimplex linha;
	for (unsigned int i = 0; i < tabela.size(); i++) {
		linha = tabela[i];
		if (linha.possuiVariavelArtificial()) {
			totalDeVariaveisArtificiais++;
		}
	}
	return totalDeVariaveisArtificiais;
}

void Simplex::defineVariaveis() {
	UtilsSimplex utils;
	RestricaoSimplex *ponteiroLinhaSimplex;
	for (unsigned int i = 0; i < this->tabela.size(); i++) {
		ponteiroLinhaSimplex = &this->tabela[i];
		string texto = ponteiroLinhaSimplex->getSinalDaRestricao();
		string sinal = "<";
		if (utils.isSinal(texto, sinal)) {
			ponteiroLinhaSimplex->setVariavelDeFolga(true);
			ponteiroLinhaSimplex->setVariavelArtificial(false);
		} else if (utils.isSinal(texto, "")) {
			ponteiroLinhaSimplex->setVariavelDeFolga(false);
			ponteiroLinhaSimplex->setVariavelArtificial(false);
		} else {
			ponteiroLinhaSimplex->setVariavelDeFolga(true);
			ponteiroLinhaSimplex->setVariavelArtificial(true);
		}
	}
}

int Simplex::quantidadeDeVariaveisDeFolgaNasLinhasAnteriores(unsigned int i) {
	int quantidadeDeVariaveisExtrasAnteriores = 0;
	for (unsigned int j = 0; j < i; j++) {
		bool linhaAnteriorPossuiVariavelDeFolga =
				tabela[j].possuiVariavelDeFolga();
		if (linhaAnteriorPossuiVariavelDeFolga) {
			quantidadeDeVariaveisExtrasAnteriores++;
		}
	}
	return quantidadeDeVariaveisExtrasAnteriores;
}

void Simplex::adicionaBase(Fracao valorDaBase, RestricaoSimplex* ponteiroLinha) {
	ponteiroLinha->adicionaValor(valorDaBase);
}

void Simplex::preencheComZeros(int tamanhoDaMaiorLinha,
		RestricaoSimplex* ponteiroLinha) {
	for (int i = ponteiroLinha->getQuantidadeDeValoresDaRestricao(); i < tamanhoDaMaiorLinha;
			i++) {
		ponteiroLinha->adicionaValor(Fracao(0, 1));
	}
}

UtilsSimplex Simplex::adiconaVariavelDeFolga(RestricaoSimplex* ponteiroLinha) {
	UtilsSimplex utils;
	if (utils.isSinal(ponteiroLinha->getSinalDaRestricao(), ">")) {
		ponteiroLinha->adicionaValor(Fracao(-1, 1));
	} else {
		ponteiroLinha->adicionaValor(Fracao(1, 1));
	}
	return utils;
}

void Simplex::preencheComZeroOValorDasVariaveisAnteriores(
		int quantidadeDeVariaveisExtrasAnteriores,
		RestricaoSimplex* ponteiroLinha) {
	for (int i = 0; i < quantidadeDeVariaveisExtrasAnteriores; i++) {
		ponteiroLinha->adicionaValor(Fracao(0, 1));
	}
}

void Simplex::adicionaVariaveisDeFolga() {
	RestricaoSimplex* ponteiroLinha;
	UtilsSimplex utils;
	int tamanhoDaMaiorLinha = getTamanhoDaMaiorLinhaDaTabela();
	for (unsigned int i = 0; i < tabela.size(); i++) {
		ponteiroLinha = &this->tabela[i];
		if (ponteiroLinha->isFuncaoObjetiva()) {
			while (ponteiroLinha->getQuantidadeDeValoresDaRestricao() < tamanhoDaMaiorLinha) {
				ponteiroLinha->adicionaValor(Fracao(0, 1));
			}
		} else {
			Fracao valorDaBase = ponteiroLinha->getValorDaBase();
			ponteiroLinha->removeLadoDireito();
			int quantidadeDeVariaveisExtrasAnteriores =
					quantidadeDeVariaveisDeFolgaNasLinhasAnteriores(i);
			preencheComZeroOValorDasVariaveisAnteriores(
					quantidadeDeVariaveisExtrasAnteriores, ponteiroLinha);
			utils = adiconaVariavelDeFolga(ponteiroLinha);
			preencheComZeros(tamanhoDaMaiorLinha, ponteiroLinha);
			adicionaBase(valorDaBase, ponteiroLinha);
		}
	}

}

void Simplex::adicionaVariaveisEZerosNasLinhas() {
	adicionaVariaveisDeFolga();
}


void Simplex::organizaTabela() {
	RestricaoSimplex linha;
	for (unsigned int i = 0; i < tabela.size(); i++) {
		linha = tabela[i];
		if (linha.isFuncaoObjetiva()) {
			this->tabela.erase(tabela.begin() + i);
			this->tabela.push_back(linha);
		}
	}
}

void Simplex::encontraBaseNegativaETransformaEmPositiva() {
	RestricaoSimplex *ponteiroLinhaSimplex;
	for (unsigned int i = 0; i < tabela.size(); i++) {
		ponteiroLinhaSimplex = &tabela[i];
		if (!ponteiroLinhaSimplex->isFuncaoObjetiva()) {
			Fracao valorDaBase = ponteiroLinhaSimplex->getValorDaBase();
			if (!valorDaBase.isPositivo()) {
				for (unsigned int j = 0;
						j < ponteiroLinhaSimplex->getValoresDaLinha().size();
						j++) {
					ponteiroLinhaSimplex->getValoresDaLinha()[j] =
							ponteiroLinhaSimplex->getValoresDaLinha()[j].multiplica(Fracao(-1, 1));
				}
			}
		}
	}
}

void Simplex::defineOsParametrosDaTabela() {
	defineVariaveis();
	encontraBaseNegativaETransformaEmPositiva();
	adicionaVariaveisEZerosNasLinhas();
	organizaTabela();
}

