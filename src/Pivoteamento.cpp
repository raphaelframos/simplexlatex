/*
 * Pivoteamento.cpp
 *
 *  Created on: 10/08/2014
 *      Author: raphael
 */

#include "Pivoteamento.h"

#include <sstream>

Pivoteamento::Pivoteamento(TabelaSimplex tabela) {
	this->tabelaSimplex = tabela;
	this->colunaDoPivo = 0;
	this->linhaDoPivo = 0;
	this->pivo = Fracao(0, 1);
}

void Pivoteamento::defineColunaDoPivo() {
	infoUtils.mostraMensagemParaDetalhes(
			"Encontra menor valor negativo na última linha e divide os números da base pelos valores positivos da "
					"coluna do menor valor negativo");
	RestricaoSimplex restricao = tabelaSimplex.getFuncaoObjetiva();
	Fracao menorValor = restricao.getValoresDaLinha()[0];
	colunaDoPivo = 0;
	for (unsigned int coluna = 1;
			coluna < restricao.getValoresDaLinha().size() - 1; coluna++) {
		if (!menorValor.isMenor(restricao.getValoresDaLinha()[coluna])) {
			menorValor = restricao.getValoresDaLinha()[coluna];
			colunaDoPivo = coluna;
		}
	}

	if (menorValor.getResultado() >= 0) {
		throw new exception();
	}
	stringstream mensagem;
	mensagem << "Coluna do pivo = " << colunaDoPivo + 1;
	infoUtils.mostraMensagemParaDetalhes(mensagem.str());
}

void Pivoteamento::definePorOrdemLexografica(
		const vector<int>& linhasCandidatasAPivo) {
	infoUtils.mostraMensagemParaDetalhes("Define pivo por ordem lexografica");
	linhaDoPivo = linhasCandidatasAPivo[linhasCandidatasAPivo.size() - 1];
}

void Pivoteamento::defineLinhaDoPivo(const vector<int>& linhasCandidatasAPivo) {
	if (linhasCandidatasAPivo.size() > 1) {
		definePorOrdemLexografica(linhasCandidatasAPivo);
	} else {
		linhaDoPivo = linhasCandidatasAPivo[0];
	}
	infoUtils.mensagem << "Linha do pivo = " << linhaDoPivo + 1;
	infoUtils.mostraMensagemParaDetalhes();
}

void Pivoteamento::definePivoELinhaDoPivo() {
	infoUtils.mostraMensagemParaDetalhes(
			"Realiza razão mínima da base/valor da coluna do pivo.");
	vector<int> posicaoDaRestricaoNaTabelaCandidataAPivo;
	RestricaoSimplex restricao;
	int NUMERO_GRANDE_QUE_SEJA_MAIOR_QUE_OS_VALORES_DA_TABELA = 1000;
	Fracao menorValorPositivo(
			NUMERO_GRANDE_QUE_SEJA_MAIOR_QUE_OS_VALORES_DA_TABELA, 1);

	Fracao baseSobrePivo;

	for (unsigned int i = 0; i < tabelaSimplex.retornaRestricoes().size() - 1;
			i++) {
		restricao = tabelaSimplex.retornaRestricoes()[i];

		if (!restricao.isFuncaoObjetiva()) {
			try {
				baseSobrePivo = restricao.getValorDaBase().divide(
						restricao.getValor(colunaDoPivo));
			} catch (int e) {
				baseSobrePivo = Fracao(
						NUMERO_GRANDE_QUE_SEJA_MAIOR_QUE_OS_VALORES_DA_TABELA,
						1);
			}

			infoUtils.mensagem
					<< restricao.getValorDaBase().getValorDemonstracao() << "/"
					<< restricao.getValoresDaLinha()[colunaDoPivo].getValorDemonstracao()
					<< " = " << baseSobrePivo.getValorDemonstracao();
			infoUtils.mostraMensagemParaDetalhes();

			if (baseSobrePivo.getResultado() > 0
					&& baseSobrePivo.getResultado()
							<= menorValorPositivo.getResultado()) {
				menorValorPositivo = baseSobrePivo;
			}
		}
	}

	for (unsigned int i = 0; i < tabelaSimplex.retornaRestricoes().size() - 1;
			i++) {
		restricao = tabelaSimplex.retornaRestricoes()[i];
		if (!restricao.isFuncaoObjetiva()) {
			try {
				baseSobrePivo = restricao.getValorDaBase().divide(
						restricao.getValor(colunaDoPivo));
			} catch (int e) {
				baseSobrePivo = Fracao(
						NUMERO_GRANDE_QUE_SEJA_MAIOR_QUE_OS_VALORES_DA_TABELA,
						1);
			}
			if (baseSobrePivo.getResultado()
					== menorValorPositivo.getResultado()) {
				posicaoDaRestricaoNaTabelaCandidataAPivo.push_back(i);
				pivo = restricao.getValoresDaLinha()[colunaDoPivo];
			}
		}
	}

	defineLinhaDoPivo(posicaoDaRestricaoNaTabelaCandidataAPivo);
	infoUtils.mensagem << "Pivo = " << pivo.getValorDemonstracao();
	infoUtils.mostraMensagemParaDetalhes();
}

void Pivoteamento::formataLinhaDoPivo() {

	infoUtils.mostraMensagemParaDetalhes(
			"Divide a linha do pivo pelo próprio pivo para o valor do pivo ser 1");

	RestricaoSimplex *ponteiroLinhaPivo =
			&tabelaSimplex.getRestricoes()[linhaDoPivo];
	if (!pivo.isIgual(Fracao(1,1))) {
		for (unsigned int i = 0;
				i < ponteiroLinhaPivo->getValoresDaLinha().size(); i++) {

			ponteiroLinhaPivo->setValorDaLinha(i,
					(ponteiroLinhaPivo->getValor(i).divide(pivo)));
		}
	}else{
		cout << "TO AKI " << pivo.getValorDemonstracao() << "ai " << pivo.isIgual(Fracao(1,1));
	}

	infoUtils.mensagem << "Linha pivo: "
			<< ponteiroLinhaPivo->getValoresDaRestricao();
	infoUtils.mostraMensagemParaDetalhes();
}

RestricaoSimplex Pivoteamento::multiplicaLinhaPelo(Fracao valor,
		RestricaoSimplex linhaPivo) {
	RestricaoSimplex linha = linhaPivo;
	for (int i = 0; i < linha.getTamanhoRealDaRestricao(); i++) {
		linha.setValorDaLinha(i,
				linha.getValoresDaLinha()[i].multiplica(valor));
	}

	return linha;
}

void Pivoteamento::zeraColunaDoPivo() {
	RestricaoSimplex* ponteiroComLinha;
	RestricaoSimplex restricaoDoPivo = tabelaSimplex.getRestricaoDoPivo();
	infoUtils.mostraMensagemParaDetalhes(
			"Inicia operações para zerar os valores da coluna do pivo");
	for (unsigned int i = 0; i < tabelaSimplex.retornaRestricoes().size();
			i++) {
		if (i != linhaDoPivo) {

			ponteiroComLinha = &tabelaSimplex.getRestricoes()[i];
			infoUtils.mensagem << "Linha " << i + 1 << " : "
					<< ponteiroComLinha->getValoresDaRestricao();
			infoUtils.mostraMensagemParaDetalhes();

			Fracao numeroASerZerado =
					ponteiroComLinha->getValoresDaLinha()[colunaDoPivo];
			numeroASerZerado = numeroASerZerado.multiplica(Fracao(-1, 1));
			RestricaoSimplex linhaPivoModificada;
			linhaPivoModificada = multiplicaLinhaPelo(numeroASerZerado,
					tabelaSimplex.getRestricaoDoPivo());
			ponteiroComLinha->somaLinha(linhaPivoModificada);
			infoUtils.mensagem << "Nova linha " << i + 1 << " : "
					<< ponteiroComLinha->getValoresDaRestricao();
			infoUtils.mostraMensagemParaDetalhes();
		}
	}
}

void Pivoteamento::buscaSolucao() {
	formataLinhaDoPivo();
	zeraColunaDoPivo();
}

bool Pivoteamento::criterioDeParadaNaoAtingido() {

	if (tabelaSimplex.existeFuncaoObjetivaArtificial()) {
		iniciaSegundaFase();
	}

	RestricaoSimplex linha = tabelaSimplex.getFuncaoObjetiva();

	for (int i = 0; i < linha.getQuantidadeDeValoresDaRestricao(); i++) {
		if (linha.getValoresDaLinha()[i].getResultado() < 0) {
			return true;
		}
	}

	cout << "FIM" << endl << endl;
	return false;
}

void Pivoteamento::defineQuemEntraNaBase() {
	InfoUtils infoUtils;
	tabelaSimplex.setColunaDeQuemEntraNaBase(colunaDoPivo);
	stringstream mensagem;
	mensagem << "Entra na base: " << tabelaSimplex.quemEntra();
	infoUtils.mostraMensagemParaDetalhes(mensagem.str());
}

void Pivoteamento::defineQuemSaiDaBase() {
	InfoUtils infoUtils;
	stringstream mensagem;
	tabelaSimplex.setLinhaDeQuemSaiDaBase(linhaDoPivo);
	mensagem << "Sai da base: " << tabelaSimplex.quemSai() << endl;
	infoUtils.mostraMensagemParaDetalhes(mensagem.str());
}

void Pivoteamento::mostraSolucao() {
	InfoUtils infoUtils;
	stringstream mensagem;
	for (unsigned int i = 0; i < tabelaSimplex.retornaRestricoes().size() - 1;
			i++) {
		//	mensagem << "Base" << tabela[i].getNomeDaBase();
		//	mensagem << "Base " << tabelaSimplex.mostraBase() << " = " << tabela[i].getValorDaBase().getValorDemonstracao() << endl;
	}
//	mensagem << "z = " << (retornaUltimaLinha().getValorDaBase().getResultado() * -1);
	infoUtils.mostraMensagemParaDetalhes(mensagem.str());
}

bool Pivoteamento::ultimaLinhaPositiva() {

	return false;
}

void Pivoteamento::iniciaSegundaFase() {

	vector<RestricaoSimplex> *ponteiroComAsRestricoes =
			&tabelaSimplex.getRestricoes();
	RestricaoSimplex funcaoObjetivaArtificial =
			tabelaSimplex.getRestricoes()[tabelaSimplex.getRestricoes().size()
					- 1];
	int quantidadeDeVariaveisArtificiais =
			tabelaSimplex.retornaQuantidadeDeVariaveisArtificiais();

	if (funcaoObjetivaArtificial.isRestricaoTodaPositiva()) {
		infoUtils.mostraMensagemParaDetalhes("\nRemove restrição artificial");
		tabelaSimplex.setVariaveisArtificiaisRemovidas(false);
		for (int j = 0; j < quantidadeDeVariaveisArtificiais; j++) {
			//	tabelaSimplex.removeRotulo(
			//	tabelaSimplex.getRotulosDasVariaveis().size() - 2);
		}
		ponteiroComAsRestricoes->erase(
				ponteiroComAsRestricoes->begin()
						+ ponteiroComAsRestricoes->size() - 1);
		tabelaSimplex.setFuncaoObjetivaArtificial(false);
		tabelaSimplex.setVariaveisArtificiaisRemovidas(true);
		for (unsigned int i = 0; i < ponteiroComAsRestricoes->size(); i++) {
			for (int j = 0; j < quantidadeDeVariaveisArtificiais; j++) {
				tabelaSimplex.getRestricoes()[i].removeValorDaRestricaoNaPosicao(
						tabelaSimplex.getRestricoes()[i].getTamanhoRealDaRestricao()
								- 2);
			}
		}
	}
}

vector<TabelaSimplex> Pivoteamento::iniciaPivoteamento() {

	infoUtils.mostraMensagemParaDetalhes("\n\nInicia pivoteamento!\n");
	tabelaSimplex.defineBase();
	cout << "Variaveis " << tabelaSimplex.mostraRotulosDasVariaveis() << endl;
	tabelaSimplex.mostraBase();
	cout << tabelaSimplex.getTabela() << endl;
	while (criterioDeParadaNaoAtingido()) {
		defineColunaDoPivo();
		definePivoELinhaDoPivo();
		defineQuemSaiDaBase();
		defineQuemEntraNaBase();
		tabelaSimplex.mostraBase();
		tabelasFinais.push_back(tabelaSimplex);
		tabelaSimplex.trocaVariavelNaBase(colunaDoPivo);
		buscaSolucao();
	}


	cout << tabelaSimplex.getTabela() << endl;
	tabelasFinais.push_back(tabelaSimplex);

	for (unsigned int i = 0; i < tabelasFinais.size(); i++) {
		cout << tabelasFinais[i].getTabela() << endl << endl;
	}
	tabelaSimplex.mostraSolucao();
	return tabelasFinais;
}

vector<TabelaSimplex> Pivoteamento::getTabelasFinais() {
	return tabelasFinais;
}

