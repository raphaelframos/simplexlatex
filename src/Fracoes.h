/*
 * Fracoes.h
 *
 *  Created on: 13/08/2014
 *      Author: raphael
 */

#ifndef FRACOES_H_
#define FRACOES_H_

#include <iostream>
#include <string>
#include <sstream>
using namespace std;


class Fracao{
public:
	Fracao();
	Fracao(float valor);
	Fracao(int numerador, int denominador);
	Fracao soma(Fracao outraFracao);
	Fracao diminui(Fracao outraFracao);
	Fracao divide(Fracao outraFracao);
	Fracao multiplica(Fracao outraFracao);
	Fracao getValor();
	int getNumerador();
	int getDenominador();
	bool isMenor(Fracao outraFracao);
	bool isPositivo();
	bool isNegativo();
	bool isIgual(Fracao outraFracao);
	string getValorDemonstracao();
	string getValorLatex();
	float getResultado();
	void mostrar();
	void defineSinalDaFracao(int, int);

private:
	int numerator, denominador;
	int gcd(int n, int d);
};

#endif /* FRACOES_H_ */
