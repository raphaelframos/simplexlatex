/*
 * Matricial.h
 *
 *  Created on: 22/08/2014
 *      Author: raphael
 */

#ifndef MATRICIAL_H_
#define MATRICIAL_H_
#include <fstream>
#include <iostream>
#include "UtilsSimplex.h"

using namespace std;

class Matricial{
private:
	vector<RestricaoSimplex> tabela;
	vector<Fracao> I;

public:
	void iniciaTratamentoMatricial(ifstream &arquivo);
};



#endif /* MATRICIAL_H_ */
