/*
 * fracoes.cpp
 *
 *  Created on: 13/08/2014
 *      Author: raphael
 */

#include "Fracoes.h"

using namespace std;

Fracao::Fracao() {
	this->numerator = 1;
	this->denominador = 1;
}

Fracao::Fracao(int numerador, int denominador) {
	defineSinalDaFracao(numerador, denominador);
}

void Fracao::defineSinalDaFracao(int numerador, int denominador) {
	if (numerador < 0 && denominador < 0) {
		numerador *= -1;
		denominador *= -1;
	} else if (numerador >= 0 && denominador < 0) {
		numerador *= -1;
		denominador *= -1;
	}
	this->numerator = numerador;
	if (denominador == 0) {
		cout << "ERROR: ATTEMPTING TO DIVIDE BY ZERO" << endl;
		throw 100;
	} else {
		this->denominador = denominador;
	}
}

/*
 In the following functions I am dividing both numerator and denominator by the gcd function.
 GCD function accepts both numerator and denominator values. If we had 2 fractions, 1/2 and 1/4
 and we passed it into the Sum, the result would be n=6 and d=8. These are the values that GCD
 function will accept, find greatest common divisor and return the integer value of 2. In my case
 am diving both numerator and denominator on the same line by the greatest common divisor. Although
 it probably would be more efficient to create a local int variable and store GCD value in it, but
 for such small program it shouldn't make any difference.
 */
Fracao Fracao::soma(Fracao otherFraction) {
	int n = numerator * otherFraction.denominador
			+ otherFraction.numerator * denominador;
	int d = denominador * otherFraction.denominador;
	return Fracao(n / gcd(n, d), d / gcd(n, d));
}

Fracao Fracao::diminui(Fracao outraFracao) {
	int n = numerator * outraFracao.denominador
			- outraFracao.numerator * denominador;
	int d = denominador * outraFracao.denominador;
	return Fracao(n / gcd(n, d), d / gcd(n, d));
}

Fracao Fracao::divide(Fracao outraFracao) {
	int n = numerator * outraFracao.denominador;
	int d = denominador * outraFracao.numerator;
	return Fracao(n / gcd(n, d), d / gcd(n, d));
}

Fracao Fracao::multiplica(Fracao outraFracao) {
	int n = numerator * outraFracao.numerator;
	int d = denominador * outraFracao.denominador;
	return Fracao(n / gcd(n, d), d / gcd(n, d));
}

Fracao Fracao::getValor() {
	return Fracao(numerator, denominador);
}

int Fracao::getNumerador() {
	return numerator;
}

int Fracao::getDenominador() {
	return denominador;
}
// I got the GCD algorithm from the following source:
// Source C#: http://www.ww.functionx.com/csharp2/examples/gcd.htm
int Fracao::gcd(int n, int d) {
	int remainder;
	while (d != 0) {
		remainder = n % d;
		n = d;
		d = remainder;
	}
	return n;
}
void Fracao::mostrar() // Display method
{
	cout << numerator << "/" << denominador << endl;

}

bool Fracao::isMenor(Fracao outraFracao) {
	return getResultado() < outraFracao.getResultado();
}

string Fracao::getValorDemonstracao() {
	stringstream valor;
	if (denominador == 1) {
		valor << numerator;
	} else {
		valor << numerator << "/" << denominador;
	}

	return valor.str();
}

string Fracao::getValorLatex() {
	stringstream resultado;

	if (denominador == 0 || numerator == 0) {
		resultado << 0;
	} else if (denominador == 1) {
		resultado << numerator;
	} else {
		resultado << "\\frac{" << getValor().getNumerador() << "}{"
				<< getValor().getDenominador() << "}";
	}
	return resultado.str();
}

bool Fracao::isPositivo() {
	return (numerator / denominador) > 0;
}

bool Fracao::isNegativo() {
	return (numerator / denominador) < 0;
}

bool Fracao::isIgual(Fracao outraFracao) {
	if (getResultado() == outraFracao.getResultado()) {
		return true;
	}
	return false;
}

float Fracao::getResultado() {
	float resultado = 0.0;
	if(denominador == 0){
		return resultado;
	}
	resultado = numerator *1.0 / denominador;
	return resultado;
}
