/*
 * Latex.h
 *
 *  Created on: 09/09/2014
 *      Author: raphael
 */

#ifndef LATEX_H_
#define LATEX_H_

#include <string>
#include <stdio.h>
#include <iostream>
#include <fstream>
#include <cstdlib>
#include <vector>
#include "Fracoes.h"
#include "TabelaSimplex.h"
#include <sstream>

using namespace std;

class Latex {

public:
	void criaArquivo(vector<TabelaSimplex>);
	string formataVariaveis(TabelaSimplex tabela, int posicaoRestricao, int posicaoVariavel);
	string criaTabelaComVariaveisExtras(TabelaSimplex tabela);
	string criaTabelas(vector<TabelaSimplex> tabelas);
	string defineQuantidadeDeColunasC(int);
private:
	string defineSinalDaVariavel(Fracao variavel);
	string defineSinalDaRestricao(RestricaoSimplex restricao);
};

#endif /* LATEX_H_ */
