/*
 * ConstantsUtils.h
 *
 *  Created on: 12/08/2014
 *      Author: raphael
 */

#ifndef CONSTANTSUTILS_H_
#define CONSTANTSUTILS_H_
using namespace std;

class ConstantsUtils {

public:
	static int getEspacoParaValores() {
		return 6;
	}
	static bool getDetalhes(){
		return true;
	}

	static int getPrimeiraPosicao(){
		return 0;
	}
};

#endif /* CONSTANTSUTILS_H_ */
