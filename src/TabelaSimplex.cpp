/*
 * TabelaSimplex.cpp
 *
 *  Created on: 27/08/2014
 *      Author: raphael
 */

#include "TabelaSimplex.h"

TabelaSimplex::TabelaSimplex(){
	possuiFuncaoObjetivaArtificial = false;
	variaveisArtificiaisRemovidas = false;
}


vector<RestricaoSimplex> TabelaSimplex::retornaRestricoes() {
	return this->restricoes;
}

vector<RestricaoSimplex> &TabelaSimplex::getRestricoes() {
	return this->restricoes;
}

int TabelaSimplex::getQuantidadeDeValores(int posicao) {
	return restricoes[posicao].getQuantidadeDeValoresDaRestricao();
}

Fracao TabelaSimplex::getValorDaRestricaoNaPosicao(int posicaoRestricao,
		int posicaoValor) {
	return restricoes[posicaoRestricao].getValoresDaLinha()[posicaoValor];
}

string TabelaSimplex::getRotuloDaVariavel(int posicao) {
	return getRotulosDasVariaveis()[posicao];
}

vector<string> TabelaSimplex::getRotulosDasVariaveis() {

	UtilsSimplex utils;
	if (rotulosDasVariaveis.size() == 0) {
		for (int i = 0; i < getQuantidadeDeValores(1); i++) {
			string resultado;
			if (i == getQuantidadeDeValores(1) - 1) {
				resultado += "";
			} else {
				resultado += "x_";
				resultado += utils.intToString(i + 1);
			}

			rotulosDasVariaveis.push_back(resultado);
		}
	}
	return rotulosDasVariaveis;
}

bool TabelaSimplex::estaNaBase(int coluna) {
	for (int j = 0; j < colunasDaBase.size(); j++) {
		if (coluna == colunasDaBase[j]) {
			return true;
		}
	}
	return false;
}

string TabelaSimplex::getSolucao() {
	stringstream mensagem;
	int contadorDeVariaveis = 0;


	for (unsigned int i = 0; i < colunasDaBase.size(); i++) {
		contadorDeVariaveis++;
		mensagem << "$" << rotulosDasVariaveis[colunasDaBase[i]] << " = "
				<< restricoes[i].getValorDaBase().getValorLatex() << "$, ";
		if (contadorDeVariaveis % 2 == 0) {
			mensagem << "\\\\";
		}else{
			mensagem << "&";
		}
	}

	for (unsigned int i = 0; i < rotulosDasVariaveis.size() - 1; i++) {
		if (!estaNaBase(i)) {
			contadorDeVariaveis++;
			mensagem << "$" << rotulosDasVariaveis[i] << " = " << "0 $, ";
			if (contadorDeVariaveis % 2 == 0) {
				mensagem << "\\\\";
			}
		}
	}

	mensagem << "$ z = "
			<< getFuncaoObjetiva().getValorDaBase().getValor().multiplica(
					Fracao(-1, 1)).getValorLatex() << "$";
	contadorDeVariaveis++;
	if (contadorDeVariaveis % 2 == 0) {
		mensagem << "\\\\";
	}

	return mensagem.str();
}

void TabelaSimplex::mostraSolucao() {
	InfoUtils infoUtils;
	stringstream mensagem;

	mensagem << "Solução:" << endl;
	for (unsigned int i = 0; i < colunasDaBase.size(); i++) {
		mensagem << rotulosDasVariaveis[colunasDaBase[i]] << " = "
				<< restricoes[i].getValorDaBase().getValorDemonstracao()
				<< endl;
	}

	for (int i = 0; i < restricoes[0].getQuantidadeDeValoresDaRestricao() - 1;
			i++) {
		if (!colunaDoRotuloEstaNaBase(i)) {

			mensagem << rotulosDasVariaveis[i] << " = " << "0" << endl;
		}
	}

	mensagem << "z = "
			<< getFuncaoObjetiva().getValorDaBase().getValor().multiplica(
					Fracao(-1, 1)).getValorDemonstracao() << endl;

	infoUtils.mostraMensagemParaDetalhes(mensagem.str());
}

string TabelaSimplex::getBaseDemonstracao(int posicao) {
	return rotulosDasVariaveis[getBases()[posicao]];
	return "";
}

int TabelaSimplex::getQuantidadeDeValoresdaMaiorRestricao() {
	int maiorValor = restricoes[0].getQuantidadeDeValoresDaRestricao();
	for (unsigned int i = 1; i < restricoes.size(); i++) {
		if (maiorValor < restricoes[i].getQuantidadeDeValoresDaRestricao()) {
			maiorValor = restricoes[i].getQuantidadeDeValoresDaRestricao();
		}
	}
	return maiorValor;
}

bool TabelaSimplex::colunaDoRotuloEstaNaBase(int colunaDoRotulo) {
	for (unsigned int coluna = 0; coluna < colunasDaBase.size(); coluna++) {
		if (colunaDoRotulo == colunasDaBase[coluna]) {
			return true;
		}
	}
	return false;
}
void TabelaSimplex::encontraVariaveisExtras() {
	RestricaoSimplex *ponteiroLinhaSimplex;
	UtilsSimplex utilsSimplex;

	for (unsigned int i = 0; i < restricoes.size(); i++) {
		ponteiroLinhaSimplex = &restricoes[i];
		string texto = ponteiroLinhaSimplex->getSinalDaRestricao();
		if (utilsSimplex.isSinal(texto, "<")) {
			ponteiroLinhaSimplex->setVariavelDeFolga(true);
			ponteiroLinhaSimplex->setVariavelArtificial(false);
		} else if (utilsSimplex.isSinal(texto, ">")) {
			ponteiroLinhaSimplex->setVariavelDeFolga(true);
			ponteiroLinhaSimplex->setVariavelArtificial(true);
		} else if (utilsSimplex.isSinal(texto, "=")) {
			ponteiroLinhaSimplex->setVariavelDeFolga(false);
			ponteiroLinhaSimplex->setVariavelArtificial(true);
		} else {
			ponteiroLinhaSimplex->setVariavelDeFolga(false);
			ponteiroLinhaSimplex->setVariavelArtificial(false);
		}
		if (ponteiroLinhaSimplex->possuiVariavelArtificial()) {
			possuiFuncaoObjetivaArtificial = true;
		}
	}
}

void TabelaSimplex::adicionaVariaveis() {
	RestricaoSimplex* ponteiroLinha;
	UtilsSimplex utilsSimplex;
	int tamanhoDaMaiorLinha = utilsSimplex.retornaTamanhoDaMaiorRestricao(
			restricoes);
	for (unsigned int i = 0; i < restricoes.size(); i++) {
		ponteiroLinha = &restricoes[i];
		if (ponteiroLinha->isFuncaoObjetiva()) {
			while (ponteiroLinha->getTamanhoRealDaRestricao()
					< tamanhoDaMaiorLinha) {
				ponteiroLinha->adicionaValor(Fracao(0, 1));
			}
		} else {
			Fracao valorDaBase = ponteiroLinha->getValorDaBase();
			ponteiroLinha->removeLadoDireito();
			int quantidadeDeVariaveisExtrasAnteriores =
					utilsSimplex.retornaQuantidadeDeVariaveisExtrasNasLinhasAnteriores(
							restricoes, i);
			utilsSimplex.preencheComZeroOValorDasVariaveisAnteriores(
					quantidadeDeVariaveisExtrasAnteriores, ponteiroLinha);
			utilsSimplex.adicionaVariavelDeFolga(ponteiroLinha);
			utilsSimplex.adicionaVariavelArtificial(ponteiroLinha);
			utilsSimplex.preencheComZeroEspacoesRestantes(tamanhoDaMaiorLinha,
					ponteiroLinha);
			utilsSimplex.adicionaBase(valorDaBase, ponteiroLinha);
		}
	}
}

vector<Fracao> TabelaSimplex::mudaSinalDoValoresDaRestricao(
		RestricaoSimplex restricaoArtificial) {
	for (unsigned int j = 0; j < restricaoArtificial.getValoresDaLinha().size();
			j++) {
		restricaoArtificial.setValorDaLinha(j,
				restricaoArtificial.getValoresDaLinha()[j].multiplica(
						Fracao(-1, 1)));
	}
	return restricaoArtificial.getValoresDaLinha();
}

void TabelaSimplex::criaFuncaoObjetivaArtificial() {
	if (possuiFuncaoObjetivaArtificial) {
		RestricaoSimplex restricaoArtificial;
		for (unsigned int i = 0; i < restricoes.size(); i++) {
			if (restricoes[i].possuiVariavelArtificial()) {
				if (restricaoArtificial.getValoresDaLinha().size() == 0) {
					restricaoArtificial.setValoresDaLinha(
							restricoes[i].getValoresDaLinha());
					int colunaDaVariavelArtificial =
							restricaoArtificial.getIndiceDaUltimaColunaComValorIgualAUm();
					restricaoArtificial.setValorDaLinha(
							colunaDaVariavelArtificial, Fracao(0, 1));
					restricaoArtificial.setValoresDaLinha(
							mudaSinalDoValoresDaRestricao(restricaoArtificial));
				} else {
					RestricaoSimplex restricaoAuxiliar;
					restricaoAuxiliar.setValoresDaLinha(
							restricoes[i].getValoresDaLinha());
					int colunaDaVariavelArtificial =
							restricaoAuxiliar.getIndiceDaUltimaColunaComValorIgualAUm();
					restricaoAuxiliar.setValorDaLinha(
							colunaDaVariavelArtificial, Fracao(0, 1));
					restricaoAuxiliar.setValoresDaLinha(
							mudaSinalDoValoresDaRestricao(restricaoAuxiliar));
					restricaoArtificial.somaLinha(restricaoAuxiliar);
				}
			}
		}
		restricoes.push_back(restricaoArtificial);
	}
}

void TabelaSimplex::setFuncaoObjetivaArtificial(bool existe) {
	this->possuiFuncaoObjetivaArtificial = existe;
}

void TabelaSimplex::organizaVariaveis() {
	encontraVariaveisExtras();
	adicionaVariaveis();
	criaFuncaoObjetivaArtificial();
}

int TabelaSimplex::retornaQuantidadeDeVariaveisDeFolga() {
	int quantidadeDeVariaveisDeFolga = 0;
	for (unsigned int i = 0; i < restricoes.size(); i++) {
		if (restricoes[i].possuiVariavelDeFolga()) {
			quantidadeDeVariaveisDeFolga++;
		}
	}
	return quantidadeDeVariaveisDeFolga;
}

int TabelaSimplex::retornaQuantidadeDeVariaveisDeFolgaBasicas() {
	int quantidadeDeVariaveisDeFolga = 0;
	UtilsSimplex utils;
	for (unsigned int i = 0; i < restricoes.size(); i++) {
		if (restricoes[i].possuiVariavelDeFolga()) {
			if (!utils.isSinal(restricoes[i].getSinalDaRestricao(), ">")) {
				quantidadeDeVariaveisDeFolga++;
			}
		}
	}
	return quantidadeDeVariaveisDeFolga;
}

int TabelaSimplex::retornaQuantidadeDeVariaveisArtificiais() {
	int quantidadeDeVariaveisArtificiais = 0;
	for (unsigned int i = 0; i < restricoes.size(); i++) {
		if (restricoes[i].possuiVariavelArtificial()) {
			quantidadeDeVariaveisArtificiais++;
		}
	}
	return quantidadeDeVariaveisArtificiais;
}

void TabelaSimplex::defineRotulos() {
	int quantidadeDeVariaveisDeFolga = retornaQuantidadeDeVariaveisDeFolga();
	int quantidadeDeVariaveisArtificiais =
			retornaQuantidadeDeVariaveisArtificiais();
	int quantidadeDeVariaveisNaoBasicas =
			getQuantidadeDeValoresdaMaiorRestricao()
					- quantidadeDeVariaveisArtificiais
					- quantidadeDeVariaveisDeFolga;
	stringstream nomeDaVariavel;
	for (int i = 1;
			i < quantidadeDeVariaveisNaoBasicas + quantidadeDeVariaveisDeFolga;
			i++) {
		nomeDaVariavel.str(string());
		nomeDaVariavel.clear();
		nomeDaVariavel << "x_" << i;
		rotulosDasVariaveis.push_back(nomeDaVariavel.str());
	}

	for (int i = 1; i <= quantidadeDeVariaveisArtificiais; i++) {
		nomeDaVariavel.str(string());
		nomeDaVariavel.clear();
		nomeDaVariavel << "x_" << i << "^a";
		rotulosDasVariaveis.push_back(nomeDaVariavel.str());
	}

	rotulosDasVariaveis.push_back("");

}

string TabelaSimplex::mostraRotulosDasVariaveis() {
	stringstream nomeDasVariaveis;
	for (unsigned int i = 0; i < rotulosDasVariaveis.size(); i++) {
		nomeDasVariaveis << rotulosDasVariaveis[i] + " ";
	}
	return nomeDasVariaveis.str();
}

void TabelaSimplex::setRestricoes(vector<RestricaoSimplex> tabela) {
	this->restricoes = tabela;
}

string TabelaSimplex::getTabela() {
	stringstream linha;
	for (unsigned int i = 0; i < restricoes.size(); i++) {
		linha << restricoes[i].getRestricaoCompleta();
		linha << endl;
	}
	return linha.str();
}

void TabelaSimplex::inverteSinalDaRestricao(UtilsSimplex utils,
		RestricaoSimplex* ponteiroLinhaSimplex) {
	if (utils.isSinal(ponteiroLinhaSimplex->getSinalDaRestricao(), ">=")) {
		ponteiroLinhaSimplex->setSinalDaRestricao("<=");
	} else if (utils.isSinal(ponteiroLinhaSimplex->getSinalDaRestricao(),
			"<=")) {
		ponteiroLinhaSimplex->setSinalDaRestricao(">=");
	} else if (utils.isSinal(ponteiroLinhaSimplex->getSinalDaRestricao(),
			">")) {
		ponteiroLinhaSimplex->setSinalDaRestricao("<");
	}

	if (utils.isSinal(ponteiroLinhaSimplex->getSinalDaRestricao(), "<")) {
		ponteiroLinhaSimplex->setSinalDaRestricao(">");
	}
}

void TabelaSimplex::encontraBaseNegativaETransformaEmPositiva() {
	UtilsSimplex utils;
	RestricaoSimplex *ponteiroLinhaSimplex;
	for (unsigned int i = 0; i < restricoes.size(); i++) {
		ponteiroLinhaSimplex = &restricoes[i];
		if (!ponteiroLinhaSimplex->isFuncaoObjetiva()) {
			Fracao valorDaBase = ponteiroLinhaSimplex->getValorDaBase();
			if (!valorDaBase.isPositivo()) {
				for (int j = 0;
						j
								< ponteiroLinhaSimplex->getQuantidadeDeValoresDaRestricao();
						j++) {
					ponteiroLinhaSimplex->atualizaValor(j,
							ponteiroLinhaSimplex->getValoresDaLinha()[j].multiplica(
									Fracao(-1, 1)));
				}
				inverteSinalDaRestricao(utils, ponteiroLinhaSimplex);
			}
		}
	}
}

void TabelaSimplex::removeUltimaRestricao() {
	restricoes.pop_back();
}

RestricaoSimplex TabelaSimplex::getRestricaoDoPivo() {
	return restricoes[linhaDaVariavelQueSaiDaBase];
}

void TabelaSimplex::trocaVariavelNaBase(int colunaQueEntra) {
	this->colunasDaBase.at(linhaDaVariavelQueSaiDaBase) = colunaQueEntra;
}

void TabelaSimplex::setColunaDeQuemEntraNaBase(int colunaQueEntra) {
	this->colunaDaVariavelQueEntraNaBase = colunaQueEntra;
}

void TabelaSimplex::setLinhaDeQuemSaiDaBase(int colunaQueSai) {
	this->linhaDaVariavelQueSaiDaBase = colunaQueSai;
}

string TabelaSimplex::quemSai() {
	return rotulosDasVariaveis[colunasDaBase[linhaDaVariavelQueSaiDaBase]];
}

string TabelaSimplex::quemEntra() {
	return rotulosDasVariaveis[colunaDaVariavelQueEntraNaBase];
}

vector<int> TabelaSimplex::getBases() {
	return this->colunasDaBase;
}

bool TabelaSimplex::existeFuncaoObjetivaArtificial() {
	return possuiFuncaoObjetivaArtificial;
}

bool TabelaSimplex::variaveisArtificiasForamRemovidas(){
	return variaveisArtificiaisRemovidas;
}

void TabelaSimplex::setVariaveisArtificiaisRemovidas(bool status){
	this->variaveisArtificiaisRemovidas = status;
}

void TabelaSimplex::mostraBase() {
	InfoUtils info;
	stringstream mensagem;
	for (unsigned int i = 0; i < colunasDaBase.size(); i++) {
		mensagem << rotulosDasVariaveis[colunasDaBase[i]] << endl;
	}
	info.mostraMensagemParaDetalhes(mensagem.str());
}

int TabelaSimplex::retornaQuantidadeDeVariaveisExtras() {
	return retornaQuantidadeDeVariaveisArtificiais()
			+ retornaQuantidadeDeVariaveisDeFolga();
}

int TabelaSimplex::retornaQuantidadeDeVariaveisExtrasBasicas() {
	return retornaQuantidadeDeVariaveisArtificiais()
			+ retornaQuantidadeDeVariaveisDeFolgaBasicas();
}

int TabelaSimplex::retornaQuantidadeDeVariaveisBasicas() {
	int quantidadeDeVariaveisExtras = retornaQuantidadeDeVariaveisExtrasBasicas();
	int quantidadeDeValoresDaRestricao =
			restricoes[0].getQuantidadeDeValoresDaRestricao();
	return quantidadeDeValoresDaRestricao - (quantidadeDeVariaveisExtras + 1);
}

void TabelaSimplex::defineBase() {

	int quantidadeDevariaveisBasicasAdicionadas = 0;
	bool todosValoresSaoValidos;
	for(int colunaBasica=0; colunaBasica < getQuantidadeDeValoresdaMaiorRestricao()-1;colunaBasica++){
		float resultado = 0;
		todosValoresSaoValidos = true;
		for(unsigned j=0;j<restricoes.size();j++){
			resultado += restricoes[j].getValoresDaLinha()[colunaBasica].getResultado();
			if(restricoes[j].getValoresDaLinha()[colunaBasica].getResultado() != 1 && restricoes[j].getValoresDaLinha()[colunaBasica].getResultado() != 0){
				todosValoresSaoValidos = false;
			}
		}
		if(resultado == 1 && todosValoresSaoValidos){
			this->colunasDaBase.push_back(colunaBasica);
			quantidadeDevariaveisBasicasAdicionadas++;
		}
	}
}

RestricaoSimplex TabelaSimplex::getFuncaoObjetivaArtificial() {
	RestricaoSimplex funcaoObjetivaArtificial;
	return funcaoObjetivaArtificial;
}

RestricaoSimplex TabelaSimplex::getFuncaoObjetiva() {
	return restricoes[restricoes.size() - 1];
}

void TabelaSimplex::removeEAdicionaFuncaoObjetivaNaTabela() {
	ConstantsUtils contants;
	RestricaoSimplex funcaoObjetiva = restricoes.at(
			contants.getPrimeiraPosicao());
	restricoes.erase(restricoes.begin() + contants.getPrimeiraPosicao());
	restricoes.push_back(funcaoObjetiva);
}

void TabelaSimplex::removeRotulo(int posicao) {
	rotulosDasVariaveis.erase(rotulosDasVariaveis.begin() + posicao);
}

void TabelaSimplex::organizaTabela() {
	removeEAdicionaFuncaoObjetivaNaTabela();
	encontraBaseNegativaETransformaEmPositiva();
	organizaVariaveis();
	defineRotulos();
}

