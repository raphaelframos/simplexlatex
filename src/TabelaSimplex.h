/*
 * TabelaSimplex.h
 *
 *  Created on: 27/08/2014
 *      Author: raphael
 */

#ifndef TABELASIMPLEX_H_
#define TABELASIMPLEX_H_

#include <string.h>
#include <iostream>
#include <string.h>
#include <string>
#include <vector>
#include "Fracoes.h"
#include "InformacoesUtils.h"
#include "RestricaoSimplex.h"
#include "UtilsSimplex.h"

using namespace std;


class TabelaSimplex{

public:
	TabelaSimplex();
	void setRestricoes(vector<RestricaoSimplex> tabela);
	vector<RestricaoSimplex> retornaRestricoes();
	RestricaoSimplex getFuncaoObjetivaArtificial();
	bool existeFuncaoObjetivaArtificial();
	bool estaNaBase(int coluna);
	void defineBase();
	vector<int> getBases();
	void mostraBase();
	void setQuemEntra(string);
	void setLinhaDeQuemSaiDaBase(int);
	void setColunaDeQuemEntraNaBase(int);
	string quemSai();
	string quemEntra();
	void removeUltimaRestricao();
	string getTabela();
	vector<RestricaoSimplex> &getRestricoes();
	RestricaoSimplex getRestricaoDoPivo();
	RestricaoSimplex getFuncaoObjetiva();
	string mostraRotulosDasVariaveis();
	void organizaTabela();
	void mostraSolucao();
	bool colunaDoRotuloEstaNaBase(int);
	void setFuncaoObjetivaArtificial(bool);
	int retornaQuantidadeDeVariaveisArtificiais();
	int getQuantidadeDeValores(int);
	Fracao getValorDaRestricaoNaPosicao(int, int);
	string getRotuloDaVariavel(int);
	string getBaseDemonstracao(int);
	vector<string> getRotulosDasVariaveis();
	void trocaVariavelNaBase(int colunaQueEntra);
	string getSolucao();
	void removeRotulo(int);
	int getQuantidadeDeValoresdaMaiorRestricao();
	bool variaveisArtificiasForamRemovidas();
	void setVariaveisArtificiaisRemovidas(bool);
	void setMaxOuMin(string);
	string getMaxOuMin();
 private:
	bool variaveisArtificiaisRemovidas;
	vector<RestricaoSimplex> restricoes;
	vector<int> colunasDaBase;
	int linhaDaVariavelQueSaiDaBase;
	int colunaDaVariavelQueEntraNaBase;
	vector<string> rotulosDasVariaveis;
	void removeEAdicionaFuncaoObjetivaNaTabela();
	void encontraBaseNegativaETransformaEmPositiva();
	void encontraVariaveisExtras();
	void organizaVariaveis();
	void adicionaVariaveis();
	void defineRotulos();

	int retornaQuantidadeDeVariaveisDeFolga();

	int retornaQuantidadeDeVariaveisExtras();
	int retornaQuantidadeDeVariaveisExtrasBasicas();
	int retornaQuantidadeDeVariaveisDeFolgaBasicas();
	int retornaQuantidadeDeVariaveisBasicas();
	vector<Fracao> mudaSinalDoValoresDaRestricao(RestricaoSimplex restricaoArtificial);
	void criaFuncaoObjetivaArtificial();
	void inverteSinalDaRestricao(UtilsSimplex utils,
			RestricaoSimplex* ponteiroLinhaSimplex);

	bool possuiFuncaoObjetivaArtificial;
	string problemaDoTipoMaxOuMin;
};



#endif /* TABELASIMPLEX_H_ */
