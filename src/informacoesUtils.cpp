/*
 * informacoesUtils.cpp
 *
 *  Created on: 12/08/2014
 *      Author: raphael
 */

#include "InformacoesUtils.h"


InfoUtils::InfoUtils(){
}

void InfoUtils::limpaMensagem() {
	mensagem.str(std::string());
	mensagem.clear();
}

void InfoUtils::mostraMensagemParaDetalhes(){
	ConstantsUtils constantsUtils;
		if(constantsUtils.getDetalhes() == true){
			cout << mensagem.str() << endl;
		}
	limpaMensagem();
}

void InfoUtils::mostraMensagemParaDetalhes(string mensagem){
	ConstantsUtils constantsUtils;
	if(constantsUtils.getDetalhes() == true){
		cout << mensagem << endl;
	}
}



