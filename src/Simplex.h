/*
 * simplex.h
 *
 *  Created on: 30/07/2014
 *      Author: raphael
 */

#ifndef SIMPLEX_H_
#define SIMPLEX_H_

#include <iostream>
#include <fstream>
#include <string>
#include <cstdlib>
#include <vector>
#include "RestricaoSimplex.h"
#include "Pivoteamento.h"
#include "InformacoesUtils.h"
#include "ConstantsUtils.h"
#include "UtilsSimplex.h"
#include <iomanip> //setw
#include "TabelaSimplex.h"

using namespace std;

class Simplex {
public:
	vector<TabelaSimplex> resolveTabelasSimplexComArquivoDefinidoNoTXT(ifstream &arquivo);
	void mostraTabela();
	InfoUtils infoUtils;
	void nomeiaVariaveis();

private:
	vector<RestricaoSimplex> tabela;
	vector<RestricaoSimplex>& getTabela();
	void adicionaLinha(RestricaoSimplex linha);
	void defineOsParametrosDaTabela();
	int getTamanhoDaMaiorLinhaDaTabela();
	void organizaTabela();
	int getTotalDeVariaveisExtras();
	int quantidadeDeLinhasComVariaveis();
	void defineVariaveis();
	void adicionaVariaveisEZerosNasLinhas();
	int quantidadeDeVariaveisDeFolgaNasLinhasAnteriores(unsigned int i);
	int quantidadeTotalDeVariaveisArtificiais();
	void adicionaVariaveisDeFolga();
	void lerArquivoSeparandoNumerosSinais(ifstream& arquivo);
	RestricaoSimplex& ultimaLinha();
	void encontraBaseNegativaETransformaEmPositiva();
	Fracao retornaElementoDaTabela(int linha, int coluna);
	void adicionaBase(Fracao valorDaBase, RestricaoSimplex* ponteiroLinha);
	void preencheComZeros(int tamanhoDaMaiorLinha, RestricaoSimplex* ponteiroLinha);
	UtilsSimplex adiconaVariavelDeFolga(RestricaoSimplex* ponteiroLinha);
	void preencheComZeroOValorDasVariaveisAnteriores(
			int quantidadeDeVariaveisExtrasAnteriores,
			RestricaoSimplex* ponteiroLinha);




	void salvaProblema(ifstream& arquivo);
	TabelaSimplex retornaUltimaTabelaAdicionada();
	void iniciaResolucaoDoProblema();

	vector<TabelaSimplex> tabelasSimplex;
	TabelaSimplex tabelaSimplex;
};

#endif /* SIMPLEX_H_ */
