/*
 * Utils.cpp
 *
 *  Created on: 01/08/2014
 *      Author: raphael
 */

#include "UtilsSimplex.h"

int UtilsSimplex::retornaQuantidadeDeVariaveisExtras(
		const vector<RestricaoSimplex>& tabela) {
	RestricaoSimplex linha;
	int quantidadeDeVariaveisExtras = 0;
	for (unsigned int i = 0; i < tabela.size(); i++) {
		linha = tabela[i];
		quantidadeDeVariaveisExtras += linha.getQuantidadeDeVariaveisExtras();
	}
	return quantidadeDeVariaveisExtras;
}

int UtilsSimplex::retornaTamanhoDaMaiorRestricao(
		vector<RestricaoSimplex> restricoes) {
	int tamanhoDaLinhaComMaisValoresSemVariaveisExtras =
			restricoes[0].getQuantidadeDeValoresDaRestricao();
	RestricaoSimplex linha;
	for (unsigned int i = 1; i < restricoes.size(); i++) {
		linha = restricoes[i];
		if (linha.getQuantidadeDeValoresDaRestricao()
				> tamanhoDaLinhaComMaisValoresSemVariaveisExtras) {
			tamanhoDaLinhaComMaisValoresSemVariaveisExtras =
					linha.getQuantidadeDeValoresDaRestricao();
		}
	}
	int quantidadeDeVariaveisExtras = retornaQuantidadeDeVariaveisExtras(
			restricoes);
	return tamanhoDaLinhaComMaisValoresSemVariaveisExtras
			+ quantidadeDeVariaveisExtras;
}

int UtilsSimplex::retornaQuantidadeDeVariaveisExtrasNasLinhasAnteriores(
		vector<RestricaoSimplex> tabela, int linhaLimite) {
	int quantidadeDeVariaveisExtrasAnteriores = 0;
	for (int j = 0; j < linhaLimite; j++) {
		bool linhaAnteriorPossuiVariavelDeFolga =
				tabela[j].possuiVariavelDeFolga();
		if (linhaAnteriorPossuiVariavelDeFolga) {
			quantidadeDeVariaveisExtrasAnteriores++;
		}

		bool linhaAnteriorPossuiVariavelArtificial =
				tabela[j].possuiVariavelArtificial();
		if (linhaAnteriorPossuiVariavelArtificial) {
			quantidadeDeVariaveisExtrasAnteriores++;
		}
	}
	return quantidadeDeVariaveisExtrasAnteriores;
}

void UtilsSimplex::preencheComZeroOValorDasVariaveisAnteriores(
		int quantidadeVariaveisExtrasNasLinhasAnteriores,
		RestricaoSimplex *ponteiroLinha) {
	for (int i = 0; i < quantidadeVariaveisExtrasNasLinhasAnteriores; i++) {
		ponteiroLinha->adicionaValor(Fracao(0, 1));
	}
}

void UtilsSimplex::adicionaVariavelDeFolga(RestricaoSimplex *ponteiroLinha) {
	if (isSinal(ponteiroLinha->getSinalDaRestricao(), ">")) {
		ponteiroLinha->adicionaValor(Fracao(-1, 1));
	} else {
		ponteiroLinha->adicionaValor(Fracao(1, 1));
	}
}

void UtilsSimplex::adicionaVariavelArtificial(RestricaoSimplex *ponteiroLinha) {
	if (isSinal(ponteiroLinha->getSinalDaRestricao(), ">")) {
		ponteiroLinha->adicionaValor(Fracao(1, 1));
	}
}

void UtilsSimplex::preencheComZeroEspacoesRestantes(int tamanhoDaMaiorLinha,
		RestricaoSimplex *ponteiroLinha) {
	for (int i = ponteiroLinha->getTamanhoRealDaRestricao() + 1;
			i < tamanhoDaMaiorLinha; i++) {
		ponteiroLinha->adicionaValor(Fracao(0, 1));
	}
}

int UtilsSimplex::retornaQuantidadeTotalDeVariaveisArtificiais(
		vector<RestricaoSimplex> tabela) {
	int totalDeVariaveisArtificiais = 0;
	for (unsigned int i = 0; i < tabela.size(); i++) {
		if (tabela[i].possuiVariavelArtificial()) {
			totalDeVariaveisArtificiais++;
		}
	}
	return totalDeVariaveisArtificiais;
}

void UtilsSimplex::organizaTabela(vector<RestricaoSimplex> &tabela) {

	vector<RestricaoSimplex> restricoesArtificias;
	for (unsigned int i = 0; i < tabela.size(); i++) {
		if (tabela[i].isFuncaoObjetiva()) {
			RestricaoSimplex linhaObjetiva = tabela[i];
			tabela.erase(tabela.begin() + i);
			tabela.push_back(linhaObjetiva);
		}
		if (tabela[i].isFuncaoObjetivaArtificial()) {
			restricoesArtificias.push_back(tabela[i].getRestricaoArtificial());
		}
	}
	RestricaoSimplex funcaoArtificial;
	for (unsigned int j = 0;
			j < restricoesArtificias[0].getValoresDaLinha().size(); j++) {
		Fracao valor(0, 1);
		for (unsigned int i = 0; i < restricoesArtificias.size(); i++) {
			valor = valor.soma(restricoesArtificias[i].getValor(j));
		}
		funcaoArtificial.adicionaValor(valor);
	}
	funcaoArtificial.setFuncaoArtificial(true);
	tabela.push_back(funcaoArtificial);
}

void UtilsSimplex::adicionaBase(Fracao valorDaBase,
		RestricaoSimplex* ponteiroLinha) {
	ponteiroLinha->adicionaValor(valorDaBase);
}

void UtilsSimplex::adicionaVariaveis(vector<RestricaoSimplex> &tabela) {
	RestricaoSimplex* ponteiroLinha;
	int tamanhoDaMaiorLinha = retornaTamanhoDaMaiorRestricao(tabela);
	for (unsigned int i = 0; i < tabela.size(); i++) {
		ponteiroLinha = &tabela[i];
		if (ponteiroLinha->isFuncaoObjetiva()) {
			while (ponteiroLinha->getTamanhoRealDaRestricao()
					< tamanhoDaMaiorLinha) {
				ponteiroLinha->adicionaValor(Fracao(0, 1));
			}
		} else {
			Fracao valorDaBase = ponteiroLinha->getValorDaBase();
			ponteiroLinha->removeLadoDireito();
			int quantidadeDeVariaveisExtrasAnteriores =
					retornaQuantidadeDeVariaveisExtrasNasLinhasAnteriores(
							tabela, i);
			preencheComZeroOValorDasVariaveisAnteriores(
					quantidadeDeVariaveisExtrasAnteriores, ponteiroLinha);
			adicionaVariavelDeFolga(ponteiroLinha);
			adicionaVariavelArtificial(ponteiroLinha);
			preencheComZeroEspacoesRestantes(tamanhoDaMaiorLinha,
					ponteiroLinha);
			adicionaBase(valorDaBase, ponteiroLinha);
		}
	}
}

void UtilsSimplex::encontraVariaveisExtrasNa(vector<RestricaoSimplex> &tabela) {
	RestricaoSimplex *ponteiroLinhaSimplex;
	for (unsigned int i = 0; i < tabela.size(); i++) {
		ponteiroLinhaSimplex = &tabela[i];
		string texto = ponteiroLinhaSimplex->getSinalDaRestricao();
		if (isSinal(texto, "<")) {
			ponteiroLinhaSimplex->setVariavelDeFolga(true);
			ponteiroLinhaSimplex->setVariavelArtificial(false);
		} else if (isSinal(texto, ">")) {
			ponteiroLinhaSimplex->setVariavelDeFolga(true);
			ponteiroLinhaSimplex->setVariavelArtificial(true);
		} else if (isSinal(texto, "=")) {
			ponteiroLinhaSimplex->setVariavelDeFolga(false);
			ponteiroLinhaSimplex->setVariavelArtificial(true);
		} else {
			ponteiroLinhaSimplex->setVariavelDeFolga(false);
			ponteiroLinhaSimplex->setVariavelArtificial(false);
		}
	}
}

void UtilsSimplex::adicionaLinha(vector<RestricaoSimplex>& tabela,
		RestricaoSimplex& novaLinha) {
	tabela.push_back(novaLinha);
}

vector<RestricaoSimplex> UtilsSimplex::defineRestricoesSeparandoNumerosESinais(
		ifstream &arquivo) {
	vector<string> vetorComElementos;
	vector<RestricaoSimplex> tabela;

	while (!arquivo.eof()) {
		for (std::string linha; getline(arquivo, linha);) {
			string::iterator it;
			split(vetorComElementos, linha, " ");
			RestricaoSimplex novaLinha;

			for (unsigned i = 0; i < vetorComElementos.size(); i++) {
				if (isSinal(vetorComElementos[i])) {
					novaLinha.setSinalDaRestricao(vetorComElementos[i]);
				}
			//	else
			//	if (isMaxOuMin(vetorComElementos[i])) {
				//	tabelaSimplex.setMaxOuMin(vetorComElementos[i]);
			//	}
				else {
					novaLinha.adicionaValor(
							stringToFracao(vetorComElementos[i]));
				}
			}

			adicionaLinha(tabela, novaLinha);
			vetorComElementos.clear();
		}
	}
	return tabela;
}

void UtilsSimplex::lerArquivoSeparandoNumerosSinais(
		vector<RestricaoSimplex> &tabela, ifstream &arquivo) {
	vector<string> vetorComElementos;
	while (!arquivo.eof()) {
		for (std::string linha; getline(arquivo, linha);) {
			string::iterator it;
			split(vetorComElementos, linha, " ");
			RestricaoSimplex novaLinha;

			for (unsigned int i = 0; i < vetorComElementos.size(); i++) {

				if (isSinal(vetorComElementos[i])) {
					novaLinha.setSinalDaRestricao(vetorComElementos[i]);
				} else {
					novaLinha.adicionaValor(
							stringToFracao(vetorComElementos[i]));
				}
			}
			adicionaLinha(tabela, novaLinha);
			vetorComElementos.clear();
		}
	}
}

bool UtilsSimplex::isSinal(string texto) {
	if ((texto.find("=") != std::string::npos)
			|| (texto.find("<") != std::string::npos)
			|| (texto.find(">") != std::string::npos)) {
		return true;
	} else {
		return false;
	}
}

bool UtilsSimplex::isMaxOuMin(string texto) {
	if ((texto.compare("MAX") != 0) || (texto.compare("MIN") != 0)
			|| (texto.compare("max") != 0) || (texto.compare("min") != 0)
			|| (texto.compare("Max") != 0) || (texto.compare("Min") != 0)) {
		return true;
	} else {
		return false;
	}
}

bool UtilsSimplex::isSinal(string texto, string sinal) {
	return (texto.find(sinal) != std::string::npos);
}

void UtilsSimplex::split(vector<string> &elementos, string texto,
		string delimiter) {

	size_t pos = 0;
	std::string token;
	while ((pos = texto.find(delimiter)) != std::string::npos) {
		token = texto.substr(0, pos);
		texto.erase(0, pos + delimiter.length());
		elementos.push_back(token);
	}
	string resultado = texto.substr(0, texto.size());
	elementos.push_back(resultado);
}

void UtilsSimplex::criaTokens(vector<string> &elementos, string texto,
		string paramentro) {
	char *tokenPonteiro;
//	tokenPonteiro = strtok(elementos, paramentro);
	while (tokenPonteiro != NULL) {
		//	tokenPonteiro = strtok(NULL, paramentro);
	}
}

float UtilsSimplex::stringToFloat(string texto) {
	return strtod(texto.c_str(), 0);
}

string UtilsSimplex::intToString(int inteiro) {
	stringstream s;
	s << inteiro;
	return s.str();
}

string UtilsSimplex::floatToString(float valor) {
	stringstream s;
	s << valor;
	return s.str();
}
Fracao UtilsSimplex::stringToFracao(string texto) {

	int n, d;
	std::string delimiter = "/";

	size_t pos = 0;
	std::string token;
	while ((pos = texto.find(delimiter)) != std::string::npos) {
		token = texto.substr(0, pos);
		n = atoi(token.c_str());
		texto.erase(0, pos + delimiter.length());
	}
	if (token.compare("") == 0) {
		n = atoi(texto.c_str());
		d = 1;
	} else {
		d = atoi(texto.c_str());
	}

	return Fracao(n, d);

}

bool UtilsSimplex::isNumber(string line) {
	char* p;
	strtol(line.c_str(), &p, 10);
	if (*p == 0) {
		return true;
	}

	if (line.find("/")) {
		return true;
	}
	return false;
}

float UtilsSimplex::divideValores(float numerador, float denominador) {
	float razao = 0;
	try {
		razao = numerador / denominador;
	} catch (int e) {
		throw 20;
	}
	return razao;
}

