//============================================================================
// Name        : simplexLatex.cpp
// Author      : Raphael Ramos, Galdino
// Version     :
// Copyright   : Your copyright notice
// Description : Simplex in C++, Ansi-style
//============================================================================

#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <iomanip>
#include <stdlib.h>
#include "Simplex.h"
#include "Matricial.h"
#include "Latex.h"

using namespace std;

template <typename T>
class saidaLATEX{
public:
		saidaLATEX();
		~saidaLATEX();
		std::string nomeARQUIVODEDADOS;
		void abreArquivoEntrada();
		void abreArquivoSaida();
		void fechaArquivoSaida();
		void fechaArquivoEntrada();
		std::ofstream saida;
		std::ifstream entrada;//(nomeARQUIVODEDADOS,ios::in);

private:
};

/////////////////////////////////////////////////////////////////////////
// Construtor
template <typename T>
saidaLATEX<T>::saidaLATEX(){}

/////////////////////////////////////////////////////////////////////////
// Destrutor
template <typename T>
saidaLATEX<T>::~saidaLATEX(){}

template <typename T>
void saidaLATEX<T>::abreArquivoEntrada(){
		entrada.open("dados.txt");
}

template <typename T>
void saidaLATEX<T>::abreArquivoSaida(){
		saida.open("resultado.tex");
}

template <typename T>
void saidaLATEX<T>::fechaArquivoSaida(){
		saida.close();
}

template <typename T>
void saidaLATEX<T>::fechaArquivoEntrada(){
		entrada.close();
}

int main() {
	saidaLATEX<unsigned> *sl = new saidaLATEX<unsigned>();
	sl->abreArquivoEntrada();
	stringstream ss;
	Simplex simplex;
	Matricial matricial;

	if (sl->entrada.is_open()) {
		vector<TabelaSimplex> tabelaFinal = simplex.resolveTabelasSimplexComArquivoDefinidoNoTXT(sl->entrada);
	//	Latex latex;
	//	latex.criaArquivo(tabelaFinal);
	}else{
		cerr << "Arquivo não pode ser aberto!";
		exit(1);
	}
	sl->fechaArquivoEntrada();



		//	system("pdflatex resultado.tex -halt-on-error");

		//	system("resultado.pdf -halt-on-error");

}
