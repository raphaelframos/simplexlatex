################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../src/Latex.cpp \
../src/Matricial.cpp \
../src/Pivoteamento.cpp \
../src/RestricaoSimplex.cpp \
../src/TabelaSimplex.cpp \
../src/UtilsSimplex.cpp \
../src/fracoes.cpp \
../src/informacoesUtils.cpp \
../src/simplex.cpp \
../src/simplexLatex.cpp 

OBJS += \
./src/Latex.o \
./src/Matricial.o \
./src/Pivoteamento.o \
./src/RestricaoSimplex.o \
./src/TabelaSimplex.o \
./src/UtilsSimplex.o \
./src/fracoes.o \
./src/informacoesUtils.o \
./src/simplex.o \
./src/simplexLatex.o 

CPP_DEPS += \
./src/Latex.d \
./src/Matricial.d \
./src/Pivoteamento.d \
./src/RestricaoSimplex.d \
./src/TabelaSimplex.d \
./src/UtilsSimplex.d \
./src/fracoes.d \
./src/informacoesUtils.d \
./src/simplex.d \
./src/simplexLatex.d 


# Each subdirectory must supply rules for building sources it contributes
src/%.o: ../src/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


